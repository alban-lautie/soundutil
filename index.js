var express = require('express');
var app = express();
app.set('port', 8000);
app.use('/script', express.static(__dirname + '/node_modules/angular/'));
app.use('/script', express.static(__dirname + '/node_modules/angular-route/'));
app.use('/script', express.static(__dirname + '/node_modules/angularfire/dist/'));
app.use('/script', express.static(__dirname + '/node_modules/firebase/'));
app.use('/script', express.static(__dirname + '/node_modules/material-design-lite/'));
app.use('/script', express.static(__dirname + '/node_modules/sweetalert2/dist/'));
app.use(express.static(__dirname + '/public'));

// Rasberry ======================================================================
require('./gpio/rasberry.js')();

// listen (start app with node server.js) ======================================
app.listen(app.get('port'));
console.log("App listening on port " + app.get('port'));
