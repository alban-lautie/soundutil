/******************* ROUTING *****************/

appBack.config(function($routeProvider) {
	return $routeProvider.when('/', {
		redirectTo : '/home'
	})
	.when('/login', {
		templateUrl : 'login.html',
		controller : 'LoginControllerBackoffice'
	})
	.when('/home', {
		templateUrl : 'home.html',
		controller : 'HomeControllerBackoffice'
	})
	.otherwise({
		redirectTo : '/home'
	});
});
