appFront.controller('scoreController', function($scope, $firebaseObject) {
  const ref = firebase.database().ref().child('playing');
  $scope.playing = $firebaseObject(ref);

  $scope.score = (cibles) => {
    return cibles.filter(i => i==1).length
  }

})
