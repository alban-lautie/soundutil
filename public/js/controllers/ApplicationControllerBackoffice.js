appBack.controller('ApplicationControllerBackoffice', function($scope, $firebaseAuth, $location) {
  const auth = $firebaseAuth()

  auth.$onAuthStateChanged((firebaseUser) => {
    $scope.isConnect = firebaseUser ? true : false

    if (firebaseUser)
      $location.path('home')
    else
      $location.path('login')
  })
})
