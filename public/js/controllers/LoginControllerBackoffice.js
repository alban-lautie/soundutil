appBack.controller('LoginControllerBackoffice', function($scope, $firebaseAuth, $location) {

	$scope.credentials = {
		mail: '',
		password: ''
	}

  const auth = $firebaseAuth()

	$scope.login = (credentials) => {
		auth.$signInWithEmailAndPassword(credentials.mail, credentials.password).then(function(res) {
		  $location.path('home')
		}).catch(function(error) {
        switch(error.code) {
          case 'auth/invalid-email':
            swal({
      			  title: 'Connexion refusée',
      			  text: 'Votre adresse mail est invalide',
      			  type: 'error'
      			})
            break
          case 'auth/wrong-password':
            swal({
      			  title: 'Connexion refusée',
      			  text: 'Votre adresse mail et/ou mot de passe sont incorrects',
      			  type: 'error'
      			})
            break
          case 'auth/network-request-failed':
            swal({
      			  title: 'Connexion échouée',
      			  text: 'Verrifiez votre connexion internet',
      			  type: 'error'
      			})
            break
        }
		})
	}

})
