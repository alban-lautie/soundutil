# SERIOUSGAMEPROJECT #

## Getting started project ##

Clone the project 
```
#!shell
git clone git@bitbucket.org:alban-lautie/seriousgameproject.git
```
 
Install librarie's 
```
#!shell
npm install 
```

## Start Application ##

Run in two terminal windows these two commands

Run stylus watcher
```
#!shell
npm test
```

Run application
```
#!shell
npm start
```

Open browser url [http://localhost:8000](http://localhost:8000)

